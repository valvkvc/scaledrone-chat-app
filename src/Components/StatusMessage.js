import React from 'react';

export default class StatusMessage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            statusMessage: '',
        };
    }

    handleChange = (event) => {
        this.setState({ statusMessage: event.target.value });
    };

    handleSubmit = (event) => {
        event.preventDefault();
        this.props.sendStatusMessage(this.state.statusMessage);
    };

    render() {
        const isStatusChanged =
            this.state.statusMessage === this.props.currentStatusMessage;

        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div className="status-dashboard">
                        <input
                            onChange={this.handleChange}
                            value={this.state.statusMessage}
                            type="text"
                            placeholder="What are you up to?"
                        />
                        <button disabled={isStatusChanged}>Update</button>
                    </div>
                </form>
            </div>
        );
    }
}
