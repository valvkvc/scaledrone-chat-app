import React from 'react';

export default class TextMessageColor extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            color: '',
        };
    }

    handleSelectColor = (event) => {
        this.setState({ color: event.target.value });
        this.props.selectTextColor(event.target.value);
    };

    render() {
        return (
            <div>
                <div className="form-text-color">
                    <label>
                        <input
                            type="radio"
                            name="textColor"
                            value="black"
                            onChange={this.handleSelectColor}
                        />
                        <div
                            className="color-div"
                            style={{ backgroundColor: 'black' }}
                        ></div>
                    </label>
                    <label>
                        <input
                            type="radio"
                            name="textColor"
                            value="red"
                            onChange={this.handleSelectColor}
                        />
                        <div
                            className="color-div"
                            style={{ backgroundColor: 'red' }}
                        ></div>
                    </label>
                    <label>
                        <input
                            type="radio"
                            name="textColor"
                            value="green"
                            onChange={this.handleSelectColor}
                        />
                        <div
                            className="color-div"
                            style={{ backgroundColor: 'green' }}
                        ></div>
                    </label>
                    <label>
                        <input
                            type="radio"
                            name="textColor"
                            value="darkcyan"
                            onChange={this.handleSelectColor}
                        />
                        <div
                            className="color-div"
                            style={{ backgroundColor: 'darkcyan' }}
                        ></div>
                    </label>
                    <label>
                        <input
                            type="radio"
                            name="textColor"
                            value="blueviolet"
                            onChange={this.handleSelectColor}
                        />
                        <div
                            className="color-div"
                            style={{ backgroundColor: 'blueviolet' }}
                        ></div>
                    </label>
                    <label>
                        <input
                            type="radio"
                            name="textColor"
                            value="darkmagenta"
                            onChange={this.handleSelectColor}
                        />
                        <div
                            className="color-div"
                            style={{ backgroundColor: 'darkmagenta' }}
                        ></div>
                    </label>
                    <label>
                        <input
                            type="radio"
                            name="textColor"
                            value="palevioletred"
                            onChange={this.handleSelectColor}
                        />
                        <div
                            className="color-div"
                            style={{ backgroundColor: 'palevioletred' }}
                        ></div>
                    </label>
                    <label>
                        <input
                            type="radio"
                            name="textColor"
                            value="orangered"
                            onChange={this.handleSelectColor}
                        />
                        <div
                            className="color-div"
                            style={{ backgroundColor: 'orangered' }}
                        ></div>
                    </label>
                    <label>
                        <input
                            type="radio"
                            name="textColor"
                            value="darkslateblue"
                            onChange={this.handleSelectColor}
                        />
                        <div
                            className="color-div"
                            style={{ backgroundColor: 'darkslateblue' }}
                        ></div>
                    </label>
                </div>
            </div>
        );
    }
}
