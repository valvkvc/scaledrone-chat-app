import React from 'react';
import TextMessage from './TextMessage';
import StatusMessage from './StatusMessage';
import Messages from './Messages';
import OnlineMemberList from './OnlineMemberList';
import TextMessageColor from './TextMessageColor';

export default function Room({
    member,
    members,
    messages,
    sendMessage,
    setStatusMessage,
}) {
    const [textColor, setTextColor] = React.useState('');
    const [isColorPickerOpen, setIsColorPickerOpen] = React.useState(false);

    const onSendTextMessage = (text) => {
        sendMessage({
            text,
            color: textColor,
            type: 'textMessage',
        });
    };

    const onSendStatusMessage = (statusMessage) => {
        sendMessage({
            statusMessage,
            type: 'statusMessage',
        });
        setStatusMessage(statusMessage);
    };

    const onSelectTextColor = (color) => {
        setTextColor(color);
        setIsColorPickerOpen(!isColorPickerOpen);
    };

    const toggleColorPicker = () => {
        setIsColorPickerOpen(!isColorPickerOpen);
    };

    let colorPicker =
        isColorPickerOpen === true ? (
            <TextMessageColor selectTextColor={onSelectTextColor} />
        ) : null;

    return (
        <div>
            <h1 className="app-title-small">CHAT-A-BIT!</h1>
            <div className="lobby">
                <div className="chat-area">
                    <Messages messages={messages} currentMember={member} />
                    <TextMessage sendTextMessage={onSendTextMessage} />
                    <div className="color-options">
                        <button
                            className={`color-picker-button-${isColorPickerOpen}`}
                            style={{ color: textColor }}
                            onClick={toggleColorPicker}
                        >
                            ABC
                        </button>
                        {colorPicker}
                    </div>
                </div>
                <div className="side-info">
                    <StatusMessage
                        sendStatusMessage={onSendStatusMessage}
                        currentStatusMessage={member.status}
                    />
                    <OnlineMemberList onlineMembers={members} />
                </div>
            </div>
        </div>
    );
}
