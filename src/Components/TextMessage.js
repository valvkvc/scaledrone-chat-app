import React from 'react';

export default class TextMessage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            text: '',
        };
    }

    handleChange = (event) => {
        this.setState({ text: event.target.value });
    };

    handleSubmit = (event) => {
        event.preventDefault();
        this.props.sendTextMessage(this.state.text);
        this.setState({ text: '' });
    };

    render() {
        const isMessageEntered = this.state.text.length > 0;

        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div className="message-input">
                        <input
                            onChange={this.handleChange}
                            value={this.state.text}
                            type="text"
                            placeholder="Enter your message and press ENTER"
                            autoFocus={true}
                        />
                        <button disabled={!isMessageEntered}>Send</button>
                    </div>
                </form>
            </div>
        );
    }
}
