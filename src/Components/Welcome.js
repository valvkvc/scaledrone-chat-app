import React from 'react';
import { withRouter } from 'react-router-dom';

class Welcome extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            avatar: '',
        };
    }

    handleUsernameChange = (event) => {
        this.setState({ username: event.target.value });
    };

    handleAvatarChange = (event) => {
        this.setState({ avatar: event.target.value });
    };

    handleSubmit = (event) => {
        event.preventDefault();
        this.props.memberDataSubmit(this.state);
        this.props.history.push('/room');
    };

    render() {
        let isLoginComplete =
            this.state.username.length > 0 && this.state.avatar.length > 0;

        return (
            <div>
                <h1 className="app-title">CHAT-A-BIT!</h1>

                <form onSubmit={this.handleSubmit}>
                    <input
                        className="username-input"
                        onChange={this.handleUsernameChange}
                        type="text"
                        placeholder="Enter your username"
                        autoFocus={true}
                    />
                    <div className="form-avatar">
                        <label>
                            <input
                                type="radio"
                                name="avatar"
                                value="/avatars/avatar1.png"
                                onChange={this.handleAvatarChange}
                            />
                            <img src={'/avatars/avatar1.png'} alt="avatar" />
                        </label>
                        <label>
                            <input
                                type="radio"
                                name="avatar"
                                value="/avatars/avatar2.png"
                                onChange={this.handleAvatarChange}
                            />
                            <img src={'/avatars/avatar2.png'} alt="avatar" />
                        </label>
                        <label>
                            <input
                                type="radio"
                                name="avatar"
                                value="/avatars/avatar3.png"
                                onChange={this.handleAvatarChange}
                            />
                            <img src={'/avatars/avatar3.png'} alt="avatar" />
                        </label>
                        <label>
                            <input
                                type="radio"
                                name="avatar"
                                value="/avatars/avatar4.png"
                                onChange={this.handleAvatarChange}
                            />
                            <img src={'/avatars/avatar4.png'} alt="avatar" />
                        </label>
                        <label>
                            <input
                                type="radio"
                                name="avatar"
                                value="/avatars/avatar5.png"
                                onChange={this.handleAvatarChange}
                            />
                            <img src={'/avatars/avatar5.png'} alt="avatar" />
                        </label>
                        <label>
                            <input
                                type="radio"
                                name="avatar"
                                value="/avatars/avatar6.png"
                                onChange={this.handleAvatarChange}
                            />
                            <img src={'/avatars/avatar6.png'} alt="avatar" />
                        </label>
                        <label>
                            <input
                                type="radio"
                                name="avatar"
                                value="/avatars/avatar7.png"
                                onChange={this.handleAvatarChange}
                            />
                            <img src={'/avatars/avatar7.png'} alt="avatar" />
                        </label>
                        <label>
                            <input
                                type="radio"
                                name="avatar"
                                value="/avatars/avatar8.png"
                                onChange={this.handleAvatarChange}
                            />
                            <img src={'/avatars/avatar8.png'} alt="avatar" />
                        </label>
                    </div>
                    <input
                        className="enter-chat-button"
                        type="submit"
                        value="LET'S CHAT"
                        disabled={!isLoginComplete}
                    />
                </form>
            </div>
        );
    }
}

export default withRouter(Welcome);
