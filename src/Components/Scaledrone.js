import React from 'react';
import Room from './Room';

export default class Scaledrone extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            messages: [],
            member: {
                username: this.props.username,
                avatar: this.props.avatar,
                status: '',
            },
            members: [],
        };
    }

    componentDidMount() {
        this.drone = new window.Scaledrone('PQ4kTgXXszeAeel1', {
            data: this.state.member,
        });

        this.drone.on('open', (error) => {
            if (error) {
                return console.error(error);
            }
            const member = { ...this.state.member };
            member.id = this.drone.clientId;

            this.setState({ member });
        });

        const room = this.drone.subscribe('observable-room');

        room.on('message', (message) => {
            if (message.data.type === 'textMessage') {
                const messages = [...this.state.messages];
                messages.push(message);

                this.setState({ messages });
            } else if (message.data.type === 'statusMessage') {
                const members = [...this.state.members];
                const id = message.clientId;
                const index = members.findIndex((member) => member.id === id);
                members[index].statusMessage = message.data.statusMessage;

                this.setState({
                    members,
                });
            }
        });

        room.on('members', (members) => {
            this.setState({ members });
        });

        room.on('member_join', (member) => {
            const members = [...this.state.members];
            members.push(member);

            this.setState({ members });

            this.onSendMessage({
                statusMessage: this.state.member.status,
                type: 'statusMessage',
            });
        });

        room.on('member_leave', ({ id }) => {
            const members = [...this.state.members];
            const index = members.findIndex((member) => member.id === id);
            members.splice(index, 1);

            this.setState({ members });
        });
    }

    onSendMessage = (message) => {
        this.drone.publish({
            room: 'observable-room',
            message,
        });
    };

    setStatusMessage = (statusMessage) => {
        const member = { ...this.state.member };
        member.status = statusMessage;

        this.setState({ member });
    };

    render() {
        return (
            <div className="App">
                <Room
                    member={this.state.member}
                    members={this.state.members}
                    messages={this.state.messages}
                    sendMessage={this.onSendMessage}
                    setStatusMessage={this.setStatusMessage}
                />
            </div>
        );
    }
}
