import React from 'react';

export default class OnlineMemberList extends React.Component {
    renderMember(onlineMember) {
        return (
            <li key={onlineMember.id}>
                <img src={onlineMember.clientData.avatar} alt="avatar" />
                <span className="username">
                    {onlineMember.clientData.username}
                </span>
                <span className="status">{onlineMember.statusMessage}</span>
            </li>
        );
    }

    render() {
        const { onlineMembers } = this.props;
        return (
            <div className="online-members">
                <span>ONLINE MEMBERS:</span>
                <ul>
                    {onlineMembers.map((onlineMember) =>
                        this.renderMember(onlineMember)
                    )}
                </ul>
            </div>
        );
    }
}
