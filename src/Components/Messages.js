import React from 'react';
import { animateScroll } from 'react-scroll';

export default class Messages extends React.Component {
    renderMessage(message) {
        const { member, data } = message;
        const { currentMember } = this.props;
        const messageFromMe = member.id === currentMember.id;
        const className = messageFromMe ? 'my-message' : 'member-message';
        return (
            <li className={className} key={message.id}>
                <img src={member.clientData.avatar} alt="avatar" />
                <div>
                    <div className="username">
                        {member.clientData.username}:
                    </div>
                    <div className="text" style={{ color: data.color }}>
                        {data.text}
                    </div>
                </div>
            </li>
        );
    }

    componentDidMount() {
        this.scrollToBottom();
    }

    componentDidUpdate() {
        this.scrollToBottom();
    }

    scrollToBottom() {
        animateScroll.scrollToBottom({
            containerId: 'message-list',
            duration: 0,
            smooth: 'linear',
        });
    }

    render() {
        const { messages } = this.props;
        return (
            <ul className="message-list" id="message-list">
                {messages.map((message) => this.renderMessage(message))}
            </ul>
        );
    }
}
