import React from 'react';
import { Route } from 'react-router-dom';
import Welcome from './Components/Welcome';
import Scaledrone from './Components/Scaledrone';
import './App.css';

export default class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            username: '',
            avatar: '',
        };
    }

    onMemberDataSubmit = (data) => {
        this.setState({ username: data.username, avatar: data.avatar });
    };

    render() {
        const displayComponent =
            this.state.username.length === 0 &&
            this.state.avatar.length === 0 ? (
                <Route path="/">
                    <div className="welcome">
                        <Welcome memberDataSubmit={this.onMemberDataSubmit} />
                    </div>
                </Route>
            ) : (
                <Route path="/room">
                    <div className="room">
                        <Scaledrone
                            username={this.state.username}
                            avatar={this.state.avatar}
                        />
                    </div>
                </Route>
            );

        return <div>{displayComponent}</div>;
    }
}
