## Scaledrone Chat App

### Valentina Vuković

This application was created using Scaledrone realtime messaging platform and React.

On start, enter your username and choose an avatar. After submitting you will enter a chat room where you can send and receive realtime messages and customize your messages color.

On the right side bar you can see members who joined chat and their status message, as well as add your own status message.

---

Avatars: https://www.vecteezy.com/vector-art/142365-family-avatar-set
Background image: https://www.toptal.com/designers/subtlepatterns/
